const express = require('express');
const session = require('express-session');
const flash = require('express-flash');
const passport = require('./lib/passport');
const router = require('./router');

const app = express();

app.use(passport.initialize());
app.use(passport.session())

const {
    PORT = 8000
} = process.env;

app.use(express.urlencoded({
    extended: false
}))

app.use(session({
    secret: 'Buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false
}))

app.use(passport.initialize());
app.use(passport.session());


app.use(flash());

app.set('view engine', 'ejs');

app.use(router);
app.listen(PORT, () => {
    console.log(`Server berjalan di PORT: ${PORT}`)
})