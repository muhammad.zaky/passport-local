const router = require('express').Router();
const restrict = require('./middleware/restrict');
const auth = require('./controllers/authController');

router.get('/', restrict, (req, res) => {
    res.render('index', {user: req.user})
})

router.get('/register', (req, res) => res.render('register'));
router.post('/register', auth.register);

router.get('/login', (req, res) => res.render('login'));
router.post('/login', auth.login);

router.get('/whoami', restrict, auth.whoami);

module.exports = router;